#!/usr/bin/env python3

import bs4
import json
import os
import random
import re
import requests
import sys
import time
from tqdm import tqdm

LOGIN_PAGE=""
USERNAME=""
PASSWORD=""

session = requests.Session()

def slugify(k):
  return re.sub(r'[^a-zA-Z0-9-_]', '-', k)

def default_choice(prompt="", yes=True):
  choice = input("{} [{}] ".format(prompt.strip(), 'Y/n' if yes else 'y/N')).strip()
  if yes and choice.lower() != 'n' and choice.lower() != 'no': return True
  if not yes and choice.lower() != 'y' and choice.lower() != 'yes': return True
  return False

def open_config_file():
  try:
    # config given
    with open(sys.argv[1]) as config_file:
      config = json.load(config_file)
      return (config['login_page'], config['username'], config['password'])
  except (FileNotFoundError, IndexError):
    # no config given
    from getpass import getpass
    login_page = input("URL to login page: ")
    username = input("Username: ")
    password = getpass()
    return (login_page, username, password)

def login(session):
  # try to open session file
  try:
    with open("session.json") as session_config_file:
      config = json.load(session_config_file)
      print("Previous config found from {}".format(time.ctime(config['time'])))
      requests.utils.add_dict_to_cookiejar(
        session.cookies,
        config['cookies']
      )
      session.max_redirects = 5 # speed it up a bit
      dash_req = session.get(config['dash'])
      if dash_req.url.split('?')[0] == LOGIN_PAGE: raise NameError('') # hack

      return dash_req.text
  except (FileNotFoundError, NameError, ValueError, requests.TooManyRedirects):
    session.cookies.set("MoodleSession", None)
    # Try to reach page
    login_req = session.post(LOGIN_PAGE, { "username": USERNAME, "password": PASSWORD })

    # bad login url
    if not login_req.ok:
      raise FileNotFoundError("Login URL incorrect\n\nGiven: {}\nError code: {}".format(LOGIN_PAGE, login_req.status_code))

    # bad credentials
    if login_req.url == LOGIN_PAGE:
      raise ValueError("Login credentials incorrect\n\nGiven username: {}".format(USERNAME))

    print("Logged in as {}".format(USERNAME))
    if default_choice("Would you like to save this session?"):
      with open("session.json", "w") as session_config_file:
        session_config = json.dump(
          {
            "cookies": requests.utils.dict_from_cookiejar(session.cookies),
            "dash": login_req.url,
            "time": time.time()
          },
          session_config_file
        )

    return login_req.text
  finally:
    session.max_redirects = 30 # requests.models.DEFAULT_REDIRECT_LIMIT

def get_homepage(session):
  while True:
    try: 
      (LOGIN_PAGE, USERNAME, PASSWORD) = open_config_file()
      home_text = login(session)
      return bs4.BeautifulSoup(home_text, "html5lib")
    except (FileNotFoundError, ValueError) as err:
      # break if incorrect config
      if len(sys.argv) and os.path.exists(sys.argv[1]):
        print("Bad login config file.")
        print(err)
        exit(1) 
      print(err)

# get courses
def get_course_list(page_content):
  courses = page_content.find_all('div', class_='coursebox')
  courses = list(map(lambda c: (c.find('a').contents[0], c.find('a')['href']), courses))

  return courses

def get_course_choice(courses):
  from math import log10
  if not len(courses):
    print("No courses found")
    exit()

  print("Found {} course{}:".format(len(courses), 's' if len(courses) > 1 else ''))

  maxlen = max(map(lambda c: len(c[0]), courses))

  for i, (title, href) in enumerate(courses):
    print("  {}. {}    {}".format(str(i).rjust(int(log10(len(courses))) + 1), title.ljust(maxlen), href))
  print("  {}. Exit".format(len(courses)))

  course_choice = -1
  while True:
    choice = input("Please input a course choice [0-{}]: ".format(len(courses)))
    try:
      course_choice = int(choice)
      if not (0 <= course_choice <= len(courses)): raise NameError
      break
    except ValueError: print("Choice not a number.")
    except NameError: print("Choice out of bounds.")

  if course_choice == len(courses):
    print("Goodbye.")
    exit()

  print("Chose `{}`".format(courses[course_choice][0]))
  return courses[course_choice]

def get_files(a):
  title = a['aria-label']
  resources = list(map(lambda b: { 'url': b['href'], 'title': b.span.contents[0] }, a.select('.section .resource a')))
  return { 'title': title, 'resources': resources }

def safe_mkdir(folder, chdir=False):
  try:
    os.mkdir(slugify(folder))
  except FileExistsError: pass
  finally:
    if chdir: os.chdir(slugify(folder))

def main():
  homepage = get_homepage(session)
  courses = get_course_list(homepage)

  while True:
    course_choice = get_course_choice(courses)
    course_req = session.get(course_choice[1])
    course_landing = bs4.BeautifulSoup(course_req.text, "html5lib")

    sections = [ get_files(a) for a in course_landing.select(".section.main") if 'hidden' not in a['class'] ]

    safe_mkdir(course_choice[0], True)

    for i, a in enumerate(sections):
      folder = slugify(str(i) + '-' + a['title'])
      print(folder)
      
      if len(a['resources']) == 0:
        print('Empty, skipping')
        continue

      safe_mkdir(folder)

      files = [os.path.splitext(f)[0] for f in os.listdir(folder)]

      for r in a['resources']:
        title = slugify(r['title'])
        if title in files: 
          print("File {} already exists, skipping".format(title))
          continue
        
        req = session.get(r['url'], allow_redirects=False)

        if not req.ok: raise FileNotFoundError("File {} not found, error {}".format(req.url, req.status_code))

        if 300 <= req.status_code < 400:
          # yay it's a redirect
          req = session.get(req.headers['location'], stream=True)
        else:
          # frick
          content = bs4.BeautifulSoup(req.text, "html5lib")

          obj = content.find('div', class_='resourcecontent')
          if obj: obj = obj.find(['audio', 'canvas', 'embed', 'iframe', 'img', 'object', 'video']) #[c for c in obj.contents if type(c) is not bs4.element.NavigableString][0];
          # small chance that this is a pop-up box, in which case
          else: obj = content.find('div', class_='resourceworkaround').a

          # if obj has an internal fallback link, we should use it
          if obj.find('a', class_='mediafallbacklink'): obj = obj.find('a', class_='mediafallbacklink')

          href = r['url']
          if obj:
            href = obj.get('src') or obj.get('data') or obj.get('href')

          print('Redirect to ' + href)
          req = session.get(href, stream=True)

        req_size = int(req.headers.get('content-length', 0))

        if req_size > 1e7 and default_choice("File {} is {} bytes. Do you wish to download? [y/N] ".format(title, req_size), False)
          continue

        req_size = req_size/(32*1024)

        with open(folder + '/' + title + os.path.splitext(req.url)[1].split('?')[0], 'wb') as res_file:
          for req_data in tqdm(req.iter_content(32*1024), total=req_size, unit='B', unit_scale=True, desc=title):
            res_file.write(req_data)
          print('Wrote ' + title)

        time.sleep(random.randint(500, 2000) / 1000) # prevent rate-limiting

    os.chdir('..')

  print("Done.")

main()
