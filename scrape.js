document.body.insertAdjacentHTML('beforeend', `<textarea>${JSON.stringify(Array.from(document.querySelectorAll('.section.main:not(.hidden)')).map(a => {
    let title = a.getAttribute('aria-label');
    let resources = Array.from(a.querySelectorAll('.section .resource a')).map(b => { return { url: b.href, title: b.querySelector('span').childNodes[0].textContent } });
    return { title, resources }
}))}</textarea>`)
